<?php

require_once dirname(__FILE__) . '/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'addon_master_register_required_plugins' );

function addon_master_register_required_plugins() {

	$plugins = array(

		/*// This is an example of how to include a plugin bundled with a theme.
		array(
			'name'               => 'TGM Example Plugin', // The plugin name.
			'slug'               => 'tgm-example-plugin', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
*/
	
		array(
			'name'         => 'Advanced Custom Fields PRO', 
			'slug'         => 'advanced-custom-fields-pro',
			'source'       => 'https://connect.advancedcustomfields.com/index.php?a=download&p=pro&k=b3JkZXJfaWQ9MTMxMjg5fHR5cGU9ZGV2ZWxvcGVyfGRhdGU9MjAxOC0wNS0xNSAxMjoyNzo1OQ==', // The plugin source.
			'required'     => true, 
		),
		array(
			'name'      => 'Menu Image, Icons made easy',
			'slug'      => 'menu-image',
			'required'  => false,
		),
		array(
			'name'      => 'LH Add Media from URL',
			'slug'      => 'lh-add-media-from-url',
			'required'  => false,
		),

	

	);

	$config = array(
		'id'           => 'addon-master',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		
	);

	tgmpa( $plugins, $config );
}
