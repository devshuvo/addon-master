<?php
/**
 * Template Name: Homepage
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Addon_Master
 */

get_header();


?>


<!-- banner starts -->
<div class="banner home-banner d-flex align-items-center" style="background-image: url(<?php the_field( 'background_image' ); ?>);">
    

    <div class="container">
        <div class="row no-gutters align-items-center">
            <div class="col-lg-6 text-center text-lg-left">
                <h1>We create awesome <br>websites & addons</h1>
                <p class="larger-txt">Pellentesque eget velit tempus, porta ante eu,<br>consectetur augue nunc mollis tincidunt felis porta.</p>
                
                <a href="#cynic-projects" class="custom-btn secondary-btn">Check our Projects</a>
                
            </div>
            <!-- End of .col-lg-5 -->

            <div class="col-lg-6">
                <div class="img-container text-center text-lg-right">                   
					<?php if ( get_field( 'hero_image') ) { ?>
						<img alt="Home banner image" class="img-fluid" src="<?php the_field( 'hero_image' ); ?>" />
					<?php } ?>
                </div>
                <!-- End of .img-container -->
            </div>
            <!-- End of .col-lg-7 -->
        </div>
        <!-- End of .row -->
    </div>
    <!-- End of .container -->
</div>
<!-- End of .banner -->

<?php if ( have_rows( 'fun_facts' ) ) : ?>
<!-- achievements starts -->
<div class="achievements section-gap theme-bg-d achievement-extra-padding-bottom">
    <div class="container">
        <div class="row">
		<?php while ( have_rows( 'fun_facts' ) ) : the_row(); ?>		
            <div class="col-4">
                <div class="counter-block d-flex align-items-center justify-content-center">
                    <div class="icon-container">
						<?php if ( get_sub_field( 'image') ) { ?>
							<img src="<?php the_sub_field( 'image' ); ?>" />
						<?php } ?>
                    </div>
                    <div class="counter-wrapper">
                        <div class="number">
                            <span class="counter"><?php the_sub_field( 'counter' ); ?></span>+
                        </div>
                       <?php the_sub_field( 'title' ); ?>
                    </div>
                    <!-- End of .counter-wrapper -->
                </div>
                <!-- End of .counter-block -->
            </div>
		<?php endwhile; ?>
        </div>
        <!-- End of .row -->
    </div>
    <!-- End of .container -->
</div>
<!-- End of .achievements -->
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>

<section class="services light-grey-bg" id="cynic-services" data-scroll-offset="165">
    <div class="floating-service-wrapper section-gap">
        <div class="container">
            <h2 class="section-title text-center">We works in 6 simple steps</h2>
            <p class="larger-txt text-center">Pellentesque eget velit tempus, porta ante eu, consectetur.</p>
            <div class="grid-wrapper">
                <div class="carousel-frame">
                
                    <div class="col-xl-4 col-lg-6">
                        <div class="service-block text-center">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/services/service-icon-1.png" alt="Logo and branding">
                            <div class="line">
                            	<div class="circle">1</div>
                            </div>
                            <h4>Logo &amp; Branding</h4>
                            <p>Our comprehensive Online Marketing strategy will boost your website and traffic hence monthly sales.</p>
                        </div>
                        <!-- End of .service-block -->
                    </div>
                    <!-- End of .col-lg-4 -->
                    
                    <div class="col-xl-4 col-lg-6">
                        <div class="service-block text-center">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/services/service-icon-2.png" alt="undefined">

                            <div class="line">
                            	<div class="circle">2</div>
                            </div>
                            <h4>Website Development</h4>
                            <p>We design professional looking yet simple websites. Our designs are search engine and user friendly.</p>
                        </div>
                        <!-- End of .service-block -->
                    </div>
                    <!-- End of .col-lg-4 -->
                    
                    <div class="col-xl-4 col-lg-6">
                        <div class="service-block text-center">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/services/service-icon-3.png" alt="undefined">
                            
                            <div class="line">
                            	<div class="circle">3</div>
                            </div>
                            <h4>Mobile App Development</h4>
                            <p>From simple Content Management System to complex eCommerce developer, we cover it all.</p>
                        </div>
                        <!-- End of .service-block -->
                    </div>
                    <!-- End of .col-lg-4 -->
                    
                    <div class="col-xl-4 col-lg-6">
                        <div class="service-block text-center">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/services/service-icon-4.png" alt="Search Engine Optimization">
                            <div class="line">
                            	<div class="circle">4</div>
                            </div>
                            <h4>Search Engine Optimization</h4>
                            <p>We design professional looking yet simple websites. Our designs are search engine and user friendly.</p>
                        </div>
                        <!-- End of .service-block -->
                    </div>
                    <!-- End of .col-lg-4 -->
                    
                    <div class="col-xl-4 col-lg-6">
                        <div class="service-block text-center">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/services/service-icon-5.png" alt="Pay-Per-Click">
                            <div class="line">
                            	<div class="circle">5</div>
                            </div>
                            <h4>Pay-Per-Click</h4>
                            <p>We design professional looking yet simple websites. Our designs are search engine and user friendly.</p>
                        </div>
                        <!-- End of .service-block -->
                    </div>
                    <!-- End of .col-lg-4 -->
                    
                    <div class="col-xl-4 col-lg-6">
                        <div class="service-block text-center">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/services/service-icon-6.png" alt="Online Marketing">
                            <div class="line">
                            	<div class="circle">6</div>
                            </div>
                            <h4>Online Marketing</h4>
                            <p>Our comprehensive Online Marketing strategy will boost your website and traffic hence monthly sales.</p>
                        </div>
                        <!-- End of .service-block -->
                    </div>
                    <!-- End of .col-lg-4 -->
                    
                </div>
                <!-- End of .row -->
            </div>
            <!-- End of .grid-wrapper -->
        </div>
        <!-- End of .container -->
    </div>
    <!-- End of .floating-service-wrapper -->
</section>

<!-- our features starts -->
<section class="features light-grey-bg" id="cynic-about" data-scroll-offset="165">
    <div class="container">

        
            <div class="features-grid">
                <div class="row align-items-center">
                    <div class="col-lg-6 order-lg-2 offset-lg-1 text-center text-lg-right">
                        <div class="img-container">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/features/features-img-1.png" alt="why choose us" class="img-fluid">
                            
                        </div>
                        <!-- End of .img-container -->
                    </div>
                    <!-- End of .col-lg-6 -->

                    <div class="col-lg-5">
                        <div class="features-content">
                            <h2 class="section-title">Reasons for choosing us</h2>
                            <p>Nunc eu augue at nunc consectetur venenatis sit amet sodales metus. Sed at gravida nisi, mattis lacinia ipsum. Pellentesque suscipit odio quis ligula maximus aliquet. Integer cursus ipsum et posuere ornare.</p>
                            

                            
            
                            
                                <a href="about.html" class="hyperlink">Learn More About Us</a>
                            
                        </div>
                        <!-- End of .features-content -->
                    </div>
                    <!-- End of .col-lg-6 -->
                </div>
                <!-- End of .row -->
            </div>
            <!-- End of .features-grid -->
        
            <div class="features-grid">
                <div class="row align-items-center">
                    <div class="col-lg-6 text-center text-lg-left">
                        <div class="img-container">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/features/features-img-2.png" alt="why choose us" class="img-fluid">
                            
                        </div>
                        <!-- End of .img-container -->
                    </div>
                    <!-- End of .col-lg-6 -->

                    <div class="col-lg-5 offset-lg-1">
                        <div class="features-content">
                            <h2 class="section-title">Donec vel tristique dui, at imperdiet nibh.</h2>
                            <p>Nunc eu augue at nunc consectetur venenatis sit amet sodales metus. Sed at gravida nisi, mattis lacinia ipsum. Pellentesque suscipit odio quis ligula maximus aliquet. Integer cursus ipsum et posuere ornare.</p>
                            

                            
            
                            
                                <a href="about.html" class="hyperlink">Learn More About Us</a>
                            
                        </div>
                        <!-- End of .features-content -->
                    </div>
                    <!-- End of .col-lg-6 -->
                </div>
                <!-- End of .row -->
            </div>
            <!-- End of .features-grid -->
         
        
    </div>
    <!-- End of .container -->
</section>
<!-- End of .features -->

<!-- Projects starts -->
<section class="projects section-gap grey-bg" id="cynic-projects" data-scroll-offset="75">
    <div class="container">
        <h2 class="section-title text-center">Our Impressive Portfolio</h2>

        <div class="item-showcase grid-wrapper__small-padding">

            
            
            <div class="row ">
                
                    <div class="col-lg-4 col-md-6 ">
                        <a href="portfolio.html" class="img-card text-center portfolio-card white-bg " data-toggle="modal" data-target="#product-modal">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/products/project-1.png" alt="product" class="img-fluid">
                            <h4>
                                <span>Cynic Classic Design</span>Web Design</h4>
                        </a>
                        <!-- End of .img-card -->
                    </div>
                    <!-- End of .col-lg-4 -->
                
                    <div class="col-lg-4 col-md-6 ">
                        <a href="portfolio.html" class="img-card text-center portfolio-card white-bg " data-toggle="modal" data-target="#product-modal">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/products/project-2.png" alt="product" class="img-fluid">
                            <h4>
                                <span>Diet Hospital</span>Social Media</h4>
                        </a>
                        <!-- End of .img-card -->
                    </div>
                    <!-- End of .col-lg-4 -->
                
                    <div class="col-lg-4 col-md-6 ">
                        <a href="portfolio.html" class="img-card text-center portfolio-card white-bg " data-toggle="modal" data-target="#product-modal">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/products/project-3.png" alt="product" class="img-fluid">
                            <h4>
                                <span>Repo Deshboard Design</span>Logo Design</h4>
                        </a>
                        <!-- End of .img-card -->
                    </div>
                    <!-- End of .col-lg-4 -->
                
                    <div class="col-lg-4 col-md-6 ">
                        <a href="portfolio.html" class="img-card text-center portfolio-card white-bg " data-toggle="modal" data-target="#product-modal">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/products/project-4.png" alt="product" class="img-fluid">
                            <h4>
                                <span>Cynic Classic Design</span>Social Media</h4>
                        </a>
                        <!-- End of .img-card -->
                    </div>
                    <!-- End of .col-lg-4 -->
                
                    <div class="col-lg-4 col-md-6 ">
                        <a href="portfolio.html" class="img-card text-center portfolio-card white-bg " data-toggle="modal" data-target="#product-modal">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/products/project-5.png" alt="product" class="img-fluid">
                            <h4>
                                <span>Diet Hospital</span>Web Design</h4>
                        </a>
                        <!-- End of .img-card -->
                    </div>
                    <!-- End of .col-lg-4 -->
                
                    <div class="col-lg-4 col-md-6 ">
                        <a href="portfolio.html" class="img-card text-center portfolio-card white-bg " data-toggle="modal" data-target="#product-modal">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/products/project-6.png" alt="product" class="img-fluid">
                            <h4>
                                <span>Repo Deshboard Design</span>Mobile App Development</h4>
                        </a>
                        <!-- End of .img-card -->
                    </div>
                    <!-- End of .col-lg-4 -->
                
            </div>
            <div class="col-12 text-center"><a href="portfolio.html" class="custom-btn secondary-btn">DISCOVER MORE WORKS</a></div>
            <!-- End of .row -->
        </div>
        <!-- End of .project-showcase -->
    </div>
    <!-- End of .container -->
</section>
<!-- End of .projects -->

<section class="team section-gap light-grey-bg" id="cynic-team" data-scroll-offset="75" data-scroll-offset-mobile="75">
    <div class="container">
        <h2 class="section-title text-center">Teamwork Makes the Dream Work</h2>
        <p class="larger-txt text-center">Ut blandit sem vitae ipsum vestibulum finibus. Pellentesque habitant morbi
            tristique senectus</p>

        <div class="grid-wrapper">
            <div class="row">
                <div class="col-md-4">
                    <a href="team.html" class="img-card team-card text-center" data-toggle="modal" data-target="#team-modal">
    <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/team/team-member-1.png" alt="team member" class="img-fluid">
    <h4>
        <span>Steve Johnson</span> Chief Exicutive Officer</h4>
</a>
<!-- End of .img-card -->
                </div>
                <!-- End of .col-md-4 -->

                <div class="col-md-4">
                     <a href="team.html" class="img-card team-card text-center" data-toggle="modal" data-target="#team-modal">
    <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/team/team-member-2.png" alt="team member" class="img-fluid">
    <h4>
        <span>Olivia Jackson</span> Chief Operating Officer</h4>
</a>
<!-- End of .img-card -->
                </div>
                <!-- End of .col-md-4 -->

                <div class="col-md-4">
                     <a href="team.html" class="img-card team-card text-center" data-toggle="modal" data-target="#team-modal">
    <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/team/team-member-3.png" alt="team member" class="img-fluid">
    <h4>
        <span>Tamerlan Aziev</span> Chief Technology Officer</h4>
</a>
<!-- End of .img-card -->
                </div>
                <!-- End of .col-md-4 -->
                
                <div class="col-md-4">
                    <a href="team.html" class="img-card team-card text-center" data-toggle="modal" data-target="#team-modal">
    <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/team/team-member-4.png" alt="team member" class="img-fluid">
    <h4>
        <span>Silvia Perry</span> Head of Marketing</h4>
</a>
<!-- End of .img-card -->
                </div>
                <!-- End of .col-md-4 -->

                <div class="col-md-4">
                    <a href="team.html" class="img-card team-card text-center" data-toggle="modal" data-target="#team-modal">
    <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/team/team-member-5.png" alt="team member" class="img-fluid">
    <h4>
        <span>John Kenny</span> Senior Design Strategist</h4>
</a>
<!-- End of .img-card -->
                </div>
                <!-- End of .col-md-4 -->

                <div class="col-md-4">
                    <a href="team.html" class="img-card team-card text-center" data-toggle="modal" data-target="#team-modal">
    <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/team/team-member-6.png" alt="team member" class="img-fluid">
    <h4>
        <span>David Schwimmer</span> Web Developer</h4>
</a>
<!-- End of .img-card -->
                </div>
                <!-- End of .col-md-4 -->

                
                    <div class="col-12 text-center">
                        <a href="team.html" class="custom-btn secondary-btn">SEE ALL TEAM MEMBERS</a>
                    </div>
                
            </div>
            <!-- End of .row -->
        </div>
        <!-- End of .grid-wrapper -->
    </div>
    <!-- End of .container -->
</section>
<!-- End of .team -->

<div class="clients">
    <div class="container">
        <div class="row align-items-center clients-carousel-wrapper owl-carousel">
            <div class="item client">
                <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/clients/client-1.png" alt="clients' logo">
            </div>
            <div class="item client">
                <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/clients/client-2.png" alt="clients' logo">
            </div>
            <div class="item client">
                <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/clients/client-3.png" alt="clients' logo">
            </div>
            <div class="item client">
                <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/clients/client-4.png" alt="clients' logo">
            </div>
            <div class="item client">
                <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/clients/client-5.png" alt="clients' logo">
            </div>
            <div class="item client">
                <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/clients/client-6.png" alt="clients' logo">
            </div>
            <div class="item client">
                <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/clients/client-7.png" alt="clients' logo">
            </div>
        </div>
        <!-- End of .clients-carousel-wrapper -->
    </div>
    <!-- End of .container -->
</div>
<!-- End of .clients -->

<section class="pricing section-gap light-grey-bg" id="cynic-pricing" data-scroll-offset="75" data-scroll-offset-mobile="75">
    <div class="container">
        <h2 class="section-title text-center">Pricing Plans for Businesses of All Sizes</h2>
        <p class="larger-txt text-center">Nulla venenatis enim vel eros blandit, sed dignissim risus porta. Nullam
            eget erat pulvinar.</p>
        <div class="grid-wrapper pricing-grid-wrapper">
            <div class="row align-items-center no-gutters">
                <div class="col-xl-6">
                    <div class="pricing-group">
                        <div class="group-title text-center">
                            FOR STARTUPS
                        </div>
                        <!-- End of .group-title -->
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <div class="pricing-block text-center  ">
    <h3 class="pricing-level">
        Basic
    </h3>
    <div class="price">
        <sup>$</sup>9
        <sub>/mo</sub>
    </div>
    <!-- End of .price -->

    <div class="facilities text-left">
        
            <div>
                <i class="far fa-envelope-open-text"></i>
                Unlimited Projects
            </div>
        
            <div>
                <i class="far fa-user-headset"></i>
                Everyday Support
            </div>
        
    </div>
    <!-- End of .facilities -->
    <a href="https://themeforest.net/item/digital-agency-html-template/20268873?s_rank=4"
        class="custom-btn secondary-btn">PURCHASE NOW</a>   
</div>
<!-- End of .pricing-block -->
                            </div>
                            <!-- End of .col-md-6 -->

                            <div class="col-md-6">
                                <div class="pricing-block text-center  ">
    <h3 class="pricing-level">
        Middle
    </h3>
    <div class="price">
        <sup>$</sup>29
        <sub>/mo</sub>
    </div>
    <!-- End of .price -->

    <div class="facilities text-left">
        
            <div>
                <i class="far fa-envelope-open-text"></i>
                Unlimited Projects
            </div>
        
            <div>
                <i class="far fa-user-headset"></i>
                Everyday Support
            </div>
        
            <div>
                <i class="far fa-cloud-upload"></i>
                Monthly Updates
            </div>
        
    </div>
    <!-- End of .facilities -->
    <a href="https://themeforest.net/item/digital-agency-html-template/20268873?s_rank=4"
        class="custom-btn secondary-btn">PURCHASE NOW</a>   
</div>
<!-- End of .pricing-block -->
                            </div>
                            <!-- End of .col-md-6 -->
                        </div>
                        <!-- End of .row -->
                    </div>
                    <!-- End of .pricing-group -->
                </div>
                <!-- End of .col-lg-6 -->

                <div class="col-xl-6">
                    <div class="pricing-group">
                        <div class="group-title text-center">
                            FOR BUSINESS
                        </div>
                        <!-- End of .group-title -->
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <div class="pricing-block text-center  active">
    <h3 class="pricing-level">
        Pro
    </h3>
    <div class="price">
        <sup>$</sup>49
        <sub>/mo</sub>
    </div>
    <!-- End of .price -->

    <div class="facilities text-left">
        
            <div>
                <i class="far fa-envelope-open-text"></i>
                Unlimited Projects
            </div>
        
            <div>
                <i class="far fa-user-headset"></i>
                Everyday Support
            </div>
        
            <div>
                <i class="far fa-cloud-upload"></i>
                Monthly Updates
            </div>
        
            <div>
                <i class="far fa-user"></i>
                Free Domain
            </div>
        
    </div>
    <!-- End of .facilities -->
    <a href="https://themeforest.net/item/digital-agency-html-template/20268873?s_rank=4"
        class="custom-btn primary-btn">PURCHASE NOW</a>   
</div>
<!-- End of .pricing-block -->
                            </div>
                            <!-- End of .col-md-6 -->

                            <div class="col-md-6">
                                <div class="pricing-block text-center  ">
    <h3 class="pricing-level">
        King
    </h3>
    <div class="price">
        <sup>$</sup>99
        <sub>/mo</sub>
    </div>
    <!-- End of .price -->

    <div class="facilities text-left">
        
            <div>
                <i class="far fa-envelope-open-text"></i>
                Unlimited Projects
            </div>
        
            <div>
                <i class="far fa-user-headset"></i>
                Everyday Support
            </div>
        
            <div>
                <i class="far fa-cloud-upload"></i>
                Monthly Updates
            </div>
        
            <div>
                <i class="far fa-user"></i>
                Free Domain
            </div>
        
            <div>
                <i class="far fa-file"></i>
                Privacy
            </div>
        
    </div>
    <!-- End of .facilities -->
    <a href="https://themeforest.net/item/digital-agency-html-template/20268873?s_rank=4"
        class="custom-btn secondary-btn">PURCHASE NOW</a>   
</div>
<!-- End of .pricing-block -->
                            </div>
                            <!-- End of .col-md-6 -->
                        </div>
                        <!-- End of .row -->
                    </div>
                    <!-- End of .pricing-group -->
                </div>
                <!-- End of .col-lg-6 -->
            </div>
            <!-- End of . -->
        </div>
        <!-- End of .grid-wrapper -->
    </div>
    <!-- End of .container -->
</section>

<section class="newsletter section-gap theme-bg-d ">
    <div class="container">
        <h2 class="text-center">Sign up for our newsletter to stay up to 
            <br>date with tech news!</h2>

        <form action="#" class="newsletter-form" method="POST">
            <div class="row justify-content-between">
                <div class="col-md">
                    <input type="text" name="fname" placeholder="Name">
                </div>
                <div class="col-md">
                    <input type="text" name="email" placeholder="Email">
                </div>

                <div class="col-md-auto">
                    <a href="#" class="custom-btn secondary-btn">SUBSCRIBE</a>
                </div>
            </div>
            <!-- End of .row -->
        </form>
        <!-- End of .newsletter-form -->

        <div class="social-icons-wrapper d-flex justify-content-center">
            <p>Follow us:</p>
            <ul class="social-icons">
                <li>
                    <a href="http://www.facebook.com/" target="_blank" rel="noopener">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="http://twitter.com/" target="_blank" rel="noopener">
                        <i class="fab fa-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="http://youtube.com/" target="_blank" rel="noopener">
                        <i class="fab fa-youtube"></i>
                    </a>
                </li>
                <li>
                    <a href="http://plus.google.com/discover" target="_blank" rel="noopener">
                        <i class="fab fa-google-plus-g"></i>
                    </a>
                </li>
            </ul>
            <!-- End of .social-icons -->
        </div>
        <!-- End of .social-icons-wrapper -->
    </div>
    <!-- End of .container -->
</section>

<!-- Section latest-news starts -->
<section class="latest-news section-gap light-grey-bg" id="cynic-news" data-scroll-offset="75">
    <div class="container">
        <h2 class="section-title text-center">Latest News</h2>
        
            <p class="larger-txt text-center">Nulla venenatis enim vel eros blandit, sed dignissim risus porta. Nullam eget erat pulvinar.</p>
        
        <div class="grid-wrapper">
            <div class="row justify-content-center">
                
                <div class="col-lg-4 col-md-6">
                    <a href="news-details.html" class="img-card news-card">
                        <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/news/homepage/news-1.png" alt="news image" class="img-fluid">
                        <div class="content ">
                            <h4>17 May, 2018
                                <span>Where Can You Find Unique My space Layouts Nowadays</span>
                            </h4>
                            
                        </div>
                        
                    </a>
                    <!-- End of .img-card -->
                </div>
                <!-- End of .col-lg-4 -->
                
                <div class="col-lg-4 col-md-6">
                    <a href="news-details.html" class="img-card news-card">
                        <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/news/homepage/news-2.png" alt="news image" class="img-fluid">
                        <div class="content ">
                            <h4>18 May, 2019
                                <span>How To Excel In A Technical Job Interview</span>
                            </h4>
                            
                        </div>
                        
                    </a>
                    <!-- End of .img-card -->
                </div>
                <!-- End of .col-lg-4 -->
                
                <div class="col-lg-4 col-md-6">
                    <a href="news-details.html" class="img-card news-card">
                        <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/news/homepage/news-3.png" alt="news image" class="img-fluid">
                        <div class="content ">
                            <h4>20 May, 2019
                                <span>Myspace Layouts The Missing Element</span>
                            </h4>
                            
                        </div>
                        
                    </a>
                    <!-- End of .img-card -->
                </div>
                <!-- End of .col-lg-4 -->
                
                <div class="col-lg-4 col-md-6">
                    <a href="news-details.html" class="img-card news-card">
                        <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/news/homepage/news-4.png" alt="news image" class="img-fluid">
                        <div class="content ">
                            <h4>27 March, 2019
                                <span>Home Audio Recording For Everyone</span>
                            </h4>
                            
                        </div>
                        
                    </a>
                    <!-- End of .img-card -->
                </div>
                <!-- End of .col-lg-4 -->
                
                <div class="col-lg-4 col-md-6">
                    <a href="news-details.html" class="img-card news-card">
                        <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/news/homepage/news-5.png" alt="news image" class="img-fluid">
                        <div class="content ">
                            <h4>24 April, 2019
                                <span>Help Finding Information <br>Online</span>
                            </h4>
                            
                        </div>
                        
                    </a>
                    <!-- End of .img-card -->
                </div>
                <!-- End of .col-lg-4 -->
                
                <div class="col-lg-4 col-md-6">
                    <a href="news-details.html" class="img-card news-card">
                        <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/news/homepage/news-6.png" alt="news image" class="img-fluid">
                        <div class="content ">
                            <h4>5 June, 2019
                                <span>Myspace Layouts The Missing Element</span>
                            </h4>
                            
                        </div>
                        
                    </a>
                    <!-- End of .img-card -->
                </div>
                <!-- End of .col-lg-4 -->
                
                <div class="col-lg-12 text-center">
                    <a href="#" class="custom-btn secondary-btn">LOAD MORE NEWS</a>
                </div>
                <!-- End of .text-center -->
            </div>
            <!-- End of .row -->

        </div>
        <!-- End of .grid-wrapper -->
    </div>
    <!-- End of .container -->
</section>
<!-- End of .latest-news -->

<!-- customer-reviews starts -->
<section class="customer-reviews section-gap grey-bg">
    <div class="container">
        <h2 class="section-title text-center">What People Think About Us</h2>
        <p class="larger-txt text-center">Nulla venenatis enim vel eros blandit, sed dignissim risus porta. Nullam
            eget erat pulvinar.</p>
        <div class="grid-wrapper">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6">
                    <div class="img-card review-card text-left white-bg">
    <div class="media align-items-center">
        <img class="img-fluid " src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/review/reviewer-1.png" alt="reviewer image">
        <div class="media-body">
            <h5>Julian Bowers</h5>
            <p>CEO at
                <a href="#">Uber</a>
            </p>
        </div>
    </div>
    <!-- End of .media -->
    <p>“Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed congue non nisi convallis viverra. Proin in nunc varius lorem mattis dictum. Duis tincidunt dignissim volutpat. Mauris sit amet mollis massa.” </p>
</div>
<!-- End of .img-card -->
                </div>
                <!-- End of .col-lg-4 -->

                <div class="col-lg-4 col-md-6">
                     <div class="img-card review-card text-left white-bg">
    <div class="media align-items-center">
        <img class="img-fluid " src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/review/reviewer-2.png" alt="reviewer image">
        <div class="media-body">
            <h5>Ronnie Hall</h5>
            <p>CTO at
                <a href="#">Microsoft</a>
            </p>
        </div>
    </div>
    <!-- End of .media -->
    <p>“Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed congue non nisi convallis viverra. Proin in nunc varius lorem mattis dictum. Duis tincidunt dignissim volutpat. Mauris sit amet mollis massa.” </p>
</div>
<!-- End of .img-card -->
                </div>
                <!-- End of .col-lg-4 -->

                <div class="col-lg-4 col-md-6">
                     <div class="img-card review-card text-left white-bg">
    <div class="media align-items-center">
        <img class="img-fluid " src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/review/reviewer-3.png" alt="reviewer image">
        <div class="media-body">
            <h5>Belle Hunt</h5>
            <p>COO at
                <a href="#">Apple</a>
            </p>
        </div>
    </div>
    <!-- End of .media -->
    <p>“Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed congue non nisi convallis viverra. Proin in nunc varius lorem mattis dictum. Duis tincidunt dignissim volutpat. Mauris sit amet mollis massa.” </p>
</div>
<!-- End of .img-card -->
                </div>
                <!-- End of .col-lg-4 -->
            </div>
            <!-- End of .row -->
        </div>
        <!-- End of .grid-wrapper -->
    </div>
    <!-- End of .container -->
</section>
<!-- End of .customer-reviews -->

<?php
get_footer();
