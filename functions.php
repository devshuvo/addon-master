<?php
define('AM_VERSION', '1.0.0');
/**
 * Addon Master functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Addon_Master
 */

if ( ! function_exists( 'addon_master_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function addon_master_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Addon Master, use a find and replace
		 * to change 'addon-master' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'addon-master', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'addon-master' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'addon_master_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'addon_master_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function addon_master_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'addon_master_content_width', 640 );
}
add_action( 'after_setup_theme', 'addon_master_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function addon_master_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'addon-master' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'addon-master' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'addon_master_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function addon_master_scripts() {
	$ver = current_time('timestamp');

	wp_register_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', null, AM_VERSION);	
	wp_register_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), AM_VERSION);
	
	wp_enqueue_style('bootstrap');
	wp_enqueue_script('bootstrap');

	wp_enqueue_style( 'addon-master', get_stylesheet_uri(), null, $ver );
	wp_enqueue_style('magnific-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css', null, $ver);
	wp_enqueue_style('owl.carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', null, $ver);
	wp_enqueue_style('addon-master-others', get_template_directory_uri() . '/assets/css/other-styles.css', null, $ver);

	wp_enqueue_script( 'easing', get_template_directory_uri() . '/assets/js/easing-1.3.js', array('jquery'), $ver, true );
	wp_enqueue_script( 'isotope', get_template_directory_uri() . '/assets/js/isotope.pkgd.min.js', array('jquery'), $ver, true );
	wp_enqueue_script( 'jquery.waypoints.min.js', get_template_directory_uri() . '/assets/js/jquery.waypoints.min.js', array('jquery'), $ver, true );
	wp_enqueue_script( 'jquery.counterup.min.js', get_template_directory_uri() . '/assets/js/jquery.counterup.min.js', array('jquery'), $ver, true );
	wp_enqueue_script( 'imagesloaded.pkgd.min.js', get_template_directory_uri() . '/assets/js/imagesloaded.pkgd.min.js', array('jquery'), $ver, true );
	wp_enqueue_script( 'owl.carousel.min.js', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), $ver, true );
	wp_enqueue_script( 'jquery.magnific-popup.min.js', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array('jquery'), $ver, true );
	//wp_enqueue_script( 'plugins.js', get_template_directory_uri() . '/assets/js/plugins.js', array('jquery'), $ver, true );
	wp_enqueue_script( 'addon-master', get_template_directory_uri() . '/assets/js/custom.js', array('jquery'), $ver, true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'addon_master_scripts' );



/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';



/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}


require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
require_once dirname(__FILE__) . '/inc/tgm-plugins.php';

add_filter( 'nav_menu_item_title', 'nav_menu_description', 10, 4 );
function nav_menu_description($title, $item, $args, $depth){
// add support for menu item descriptions
	if (strlen($item->description)>2) {
	    $item_output = '<span class="menu_desc">' . $item->description . '</span>';
	}

	return $title.$item_output;
}

/**
 * Changes the class on the custom logo in the header.php
 */
function helpwp_custom_logo_output( $html ) {
	$html = str_replace('custom-logo-link', 'navbar-brand', $html );
	return $html;
}
add_filter('get_custom_logo', 'helpwp_custom_logo_output', 10);