<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Addon_Master
 */

?>



<!-- location starts -->
<section class="location section-gap light-grey-bg" id="cynic-contact" data-scroll-offset="75" data-scroll-offset-mobile="75">
    <div class="container">
        <h2 class="section-title text-center">Our Location</h2>
        <ul class="nav nav-tabs location-tab justify-content-center" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#location-tab-1">Los Angeles</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#location-tab-2">Miami</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#location-tab-3">San Francisco</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#location-tab-4">New York</a>
            </li>
        </ul>
        <!-- End of .nav-tabs -->

        <div class="tab-content location-tab-content grid-wrapper" id="myTabContent">
            <div class="tab-pane fade show active" id="location-tab-1">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d423283.4355495124!2d-118.69191514099776!3d34.020730497983934!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2c75ddc27da13%3A0xe22fdf6f254608f4!2sLos+Angeles%2C+CA%2C+USA!5e0!3m2!1sen!2sbd!4v1531805451146"
                    width="600" height="450" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="tab-pane fade" id="location-tab-2">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d114964.38943832612!2d-80.29949838161122!3d25.78254531076632!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88d9b0a20ec8c111%3A0xff96f271ddad4f65!2sMiami%2C+FL%2C+USA!5e0!3m2!1sen!2sbd!4v1531805857653"
                    width="600" height="450" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="tab-pane fade" id="location-tab-3">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d42437.17344437831!2d-122.48127727514952!3d37.76280447210564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sbd!4v1531805888648"
                    width="600" height="450" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="tab-pane fade" id="location-tab-4">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d193595.1583088354!2d-74.11976389828038!3d40.697663748695746!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sbd!4v1531805909949"
                    width="600" height="450" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <!-- End of .tab-content -->

        <div class="contact-info">
            <div class="row justify-content-between">
                <div class="col-lg-4 col-md-6">
                    <div class="info">
                        <h5>Head Quarter</h5>
                        <p>868 S. La Sierra Drive <br>Los Angeles <br>California, USA</p>
                    </div>
                    <!-- End of .info -->
                </div>
                <!-- End of .col-md-3 -->
                <div class="col-lg-3 col-md-6">
                    <div class="info">
                        <h5>Email</h5>
                        <a href="mailto:info@companyname.com">info@companyname.com</a>
                    </div>
                    <!-- End of .info -->
                </div>
                <!-- End of .col-md-3 -->
                <div class="col-lg-3 col-md-6">
                    <div class="info">
                        <h5>Phone</h5>
                        <a href="tel:+12224005555">+1 (222) 400-5555</a>
                    </div>
                    <!-- End of .info -->
                </div>
                <!-- End of .col-md-3 -->
                <div class="col-md-6 col-lg-2 text-lg-right info">
                    <a href="#" class="custom-btn secondary-btn" data-toggle="modal" data-target="#quote-modal">Contact
                        Us</a>
                </div>
            </div>
            <!-- End of .contact-in -->
        </div>
        <!-- End of .contact-info -->
    </div>
    <!-- End of .container -->
</section>
<!-- End of .location -->

<div class="modal fade team-modal" id="team-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
                <!-- End of .close -->
            </div>
            <!-- End of .modal-header -->

            <div class="modal-body text-center">
                <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/team/team-modal-img.png" alt="team modal image" class="img-fluid modal-feat-img">
                <div class="modal-title">
                    <h4>Steve Johnson
                        <span>Chief Exicutive Officer</span>
                    </h4>
                </div>
                <!-- End of .modal-title -->
                <p>To be fair, in certain contexts, your professional bio does need to be more formal, like Mr. Erickson's up there. But in many cases, writing a bio that's readable  even conversational is actually a really good thing. But once created, this bio should represent who you are in the eyes.</p>

                <ul class="social-icons">
                    <li>
                        <a href="http://www.behance.net/" target="_blank" rel="noopener">
                            <i class="fab fa-behance"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://twitter.com/" target="_blank" rel="noopener">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://plus.google.com/discover" target="_blank" rel="noopener">
                            <i class="fab fa-google-plus-g"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://dribbble.com/" target="_blank" rel="noopener">
                            <i class="fab fa-dribbble"></i>
                        </a>
                    </li>
                </ul>
                <!-- End of .social-icons -->
            </div>
            <!-- End of .modal-body -->
        </div>
        <!-- End of .modal-content -->
    </div>
    <!-- End of .modal-dialog -->
</div>
<!-- End of .team-modal -->

<!-- Product Modal Starts -->
<div class="modal fade full-width-modal product-modal" id="product-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
                <!-- End of .close -->
            </div>
            <!-- End of .modal-header -->

            <div class="modal-body">
                <div class="row no-gutters">
                    <div class="col-lg-6">
                        <div class="modal-img text-center">
                            <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/products/portfolio-modal.png" alt="product-modal" class="img-fluid">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="modal-body__inner-content">
                            <h4>
                                <span>Chief Exicutive Officer</span>
                                Creative Web Design
                            </h4>
                            <p>In the early years of the commercial web, we were all Web Designers. Digital interactions, at that stage, were not incredibly sophisticated: most websites were structured as a set of individual pages connected to each other via buttons and links.</p>

                            <p>In more complex websites and information-heavy systems, the web designer would pair with an Information Architect to make sure content was organized in a way that made for that.</p>
                            <a href="about.html" class="hyperlink">Launch Website</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of .modal-body -->
        </div>
        <!-- End of .modal-content -->
    </div>
    <!-- End of .modal-dialog -->
</div>
<!-- End of .portfolio-modal -->

<!-- Quote modal starts
    ================================= -->
<div class="modal fade full-width-modal quote-modal" id="quote-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content white-bg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
                <!-- End of .close -->
            </div>
            <!-- End of .modal-header -->

            <div class="modal-body d-flex align-items-center justify-content-center text-center">
                <div class="quote-form-wrapper text-center">
                    <h3>Get a Free Quote</h3>
                    <form method="POST" class="quote-form text-center row" action="#">
                        <div class="col-lg-6">
                            <input type="text" name="fname" placeholder="Name">
                        </div>
                        <div class="col-lg-6">
                            <input type="text" name="email" placeholder="Email">
                        </div>

                        <div class="col-lg-6">
                            <input type="text" name="phone" placeholder="Phone">
                        </div>
                        <div class="col-lg-6">
                            <input type="text" name="website" placeholder="Website">
                        </div>
                        <div class="col-lg-12">
                            <textarea placeholder="Message" name="message"></textarea>
                            <button type="submit" class="custom-btn secondary-btn w-100">GET A QUOTE</button>
                            <div class="social-icons-wrapper d-flex justify-content-center">
                                <p>Follow us:</p>
                                <ul class="social-icons">
                                    <li>
                                        <a href="http://www.facebook.com/" target="_blank" rel="noopener">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://twitter.com/" target="_blank" rel="noopener">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://youtube.com/" target="_blank" rel="noopener">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://plus.google.com/discover" target="_blank" rel="noopener">
                                            <i class="fab fa-google-plus-g"></i>
                                        </a>
                                    </li>
                                </ul>
                                <!-- End of .social-icons -->
                            </div>
                            <!-- End of .social-icons-wrapper -->
                        </div>
                    </form>
                    <!-- End of .quote-form -->
                </div>
                <!-- End of .quote-form-wrapper -->
            </div>
            <!-- End of .modal-body -->
        </div>
        <!-- End of .modal-content -->
    </div>
    <!-- End of .modal-dialog -->
</div>
<!-- End of .quote-modal -->

<!-- footer starts -->
<footer class="page-footer dark-footer-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="footer-widget widget-about">
                    <a href="index.html"><img class="footer-logo" src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/footer-logo.svg" alt="footer logo"
                            height="16"></a>
                    <p>Ut wisi enim ad minim veniam. Quis nostrud exerci tation ullam corper suscipit lobortis nisl ut
                        aliquip ex eading.</p>
                    <ul class="social-icons">
                        <li>
                            <a href="http://www.facebook.com/" target="_blank" rel="noopener">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://twitter.com/" target="_blank" rel="noopener">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://youtube.com/" target="_blank" rel="noopener">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://plus.google.com/discover" target="_blank" rel="noopener">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- End of .social-icons -->
                    <p class="copywrite-txt">© 2019 <a href="http://www.axilthemes.com">Axilthemes</a>. All Rights
                        Reserved.</p>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="footer-widget">
                    <h5 class="footer-nav-title">Services</h5>
                    <ul class="footer-nav dynamic-nav">
                        <li><a href="website-design.html">Website Design</a></li>
                        <li><a href="logo-and-branding.html">Logo &amp; Branding </a></li>
                        <li><a href="mobile-app-development.html">Mobile App Development</a></li>
                        <li><a href="search-engine-optimization.html">Search Engine Optimization</a></li>
                        <li><a href="pay-per-click.html">Pay-Per-Click</a></li>
                        <li><a href="social-media-marketing.html">Social Media Marketing</a></li>
                    </ul>
                </div>
            </div>
            <!-- End of .col-lg-2 -->

            <div class="col-lg-3">
                <div class="footer-widget">
                    <h5 class="footer-nav-title">Support</h5>
                    <ul class="footer-nav dynamic-nav">
                        <li><a href="contact.html">Contact</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms &amp; Conditions</a></li>
                    </ul>
                </div>
            </div>
            <!-- End of .col-lg-2 -->

            <div class="col-lg-2">
                <div class="footer-widget">
                    <h5 class="footer-nav-title">Resources</h5>
                    <ul class="footer-nav dynamic-nav">
                        <li><a href="portfolio.html">Portfolio</a></li>
                        <li><a href="case-studies.html">Case Studies</a></li>
                        <li><a href="about.html">About</a></li>
                        <li><a href="team.html">Team</a></li>
                        <li><a href="news.html">News</a></li>
                    </ul>
                </div>
            </div>
            <!-- End of .col-lg-2 -->
        </div>
        <!-- End of .row -->
    </div>
    <!-- End of .container -->
</footer>
<!-- End of .page-footer -->


<?php wp_footer(); ?>

</body>
</html>
