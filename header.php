<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Addon_Master
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="page-header">
    <div class="container">
        <nav class="navbar navbar-expand-lg align-items-center">
            <!-- <a class="navbar-brand" href="index.html">
                <img src="http://axilthemes.com/templates/cynic/illustrated-small-digital-agency/assets/images/brand-logo.svg" alt="Cynic brand logo">
            </a> -->
					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>

					<?php } else {
						the_custom_logo();
					} ?><!-- end custom logo -->
            <!-- End of .navbar-brand -->

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#custom-navbar"
                aria-controls="custom-navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="custom-toggler-icon"></span>
                <span class="custom-toggler-icon"></span>
                <span class="custom-toggler-icon"></span>
            </button>
            <!-- End of .navbar-toggler -->

            <div class="collapse navbar-collapse" id="custom-navbar">
							<?php
							wp_nav_menu( array(
							    'theme_location'	=> 'primary',
							    'depth'				=> 2, // 1 = with dropdowns, 0 = no dropdowns.
									'container'			=> 'ul',
									'menu_class'		=> 'navbar-nav ml-auto align-items-center dynamic-nav',
							    'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
							    'walker'			=> new WP_Bootstrap_Navwalker()
							) );
							?>
            </div>
            <!-- End of .navbar-nav -->
        </nav>
    </div>
    <!-- End of .container -->
</header>
<!-- End of .page-header -->